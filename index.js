require('dotenv').config()
const Discord = require('discord.js')
const bot = new Discord.Client()
const emailValidator = require("email-validator")
const TOKEN = process.env.TOKEN

bot.login(TOKEN)

bot.on('ready', () => {
  console.info(`Logged in as ${bot.user.tag}!`)
});

bot.on('message', async (msg) => {
  // ignore wrong channel
  if (msg.channel.name !== process.env.CHANNEL_NAME) return console.info('wrong channel')
  // prevent process bot message 
  if (msg.author.username !== process.env.BOT_NAME) {
    if (msg.content.startsWith('!regis')) {
      let msgArr = msg.content.split(' ')
      if (msgArr.length !== 4) {
        msg.author.send('Please send with valid format')
        msg.author.send(`our valid format is '${process.env.REGISTER_FORMAT}'`)
        return
      }
      if (!emailValidator.validate(msgArr[3])) {
        msg.author.send('Please insert valid email')
        msgArr.splice(0, 1)
        msg.author.send(`your last registration data is "${msgArr.join(' ')}"`)
      }
    }
  }

});
